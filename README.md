# generator-reekoh-node
Yeoman generator for Reekoh Node.js Plugins.

#### Installation
`npm install --global yo generator-reekoh-node`

#### How to use?
1. In your terminal, locate the destination folder of your plugin seed. `cd /path/to/folder`
2. Run `yo reekoh-node`. (A list of plugin seeds will be displayed)
3. Select your desired plugin seed from the list.
    - `Channel`
    - `Connector`
    - `Exception Logger`
    - `Gateway`
    - `Inventory Sync`
    - `Logger`
    - `Service`
    - `Storage`
    - `Stream`
4. Press {enter} key


*You can also run `yo reekoh-node:<seed-name>` to bypass the selection. e.g. `yo reekoh-node:inventory-sync`*