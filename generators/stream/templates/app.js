'use strict'

let reekoh = require('reekoh')
let plugin = new reekoh.plugins.Stream()
let client = null

/**
 * Emitted when the platform issues a sync request. Means that the stream plugin should fetch device data from the 3rd party service.
 */
plugin.on('sync', () => {
  /*
   * When fetching data:
   * 1. Verify if the device id is registered by calling plugin.requestDeviceInfo and passing the 'device' identifier
   * 2. If the device is registered, forward the data by using plugin.pipe()
   *
   * Sample code:
   *
   * client.fetchData((err, dataCollection) => {
   *   dataCollection.forEach((data) => {
   *     plugin.requestDeviceInfo(data.deviceId)
   *       .then(() => {
   *         plugin.pipe(data)
   *       })
   *       .catch((err) => {
   *         plugin.logException(err)
   *       })
   *  })
   * })
   */
})

/**
 * Emitted when a message or command is received from the platform.
 * The message object has the following properties:
 * - device {String} - which is the ID of the target device
 * - messageId {String} - which is the ID of the message in the Reekoh database
 * - message {String} - which is the message/command itself
 *
 * @param message {Object} - The message which came from an Application or another Device from within the same pipeline as the stream.
 */
plugin.on('command', function (message) {
  // TODO: Send the message to the target client/device. These messages may contain data or commands.
  console.log(message)
})

/**
 * Emitted when the platform bootstraps the plugin. The plugin should listen once and execute its init process.
 */
plugin.once('ready', () => {
  /*
   * Initialize your stream using the plugin.config. See config.json
   * You can customize config.json based on the needs of your plugin.
   * Reekoh will inject these configuration parameters as plugin.config when the platform bootstraps the plugin.
   *
   * Note: Config Names are based on what you specify on the config.json.
   */

  // TODO: Initialize your client or subscribe to the 3rd party service here.

  /*
   * Sample Code
   *
   * let service = require('service')
   *
   * service.connect(plugin.config, (error, serviceClient) => {
   *  client = serviceClient
   * });
   */

  /*
   * When incoming data is received, forward it by using plugin.pipe()
   */
  console.log(plugin.config)
  plugin.log('Stream has been initialized.')
})

/**
 * Emitted when a new device has been registered on the platform.
 * @param device {Object} - The device details.
 */
plugin.on('adddevice', (device) => {

  // TODO: Add device to 3rd party service

  /**
   * Example:
   *
   * client.addDevice(device, () => {
   *   console.log('New device added')
   * })
   *
   */

})

/**
 * Emitted when a new device has been updated on the platform.
 * @param device {Object} - The device details.
 */
plugin.on('updatedevice', (device) => {

  // TODO: Update device from 3rd party service

  /**
   * Example:
   *
   * client.updateDevice(device, () => {
   *   console.log(`${device.name} has been updated`)
   * })
   *
   */

})

/**
 * Emitted when a new device has been removed from the platform.
 * @param device {Object} - The device details.
 */
plugin.on('removedevice', (device) => {

  // TODO: Remove device from 3rd party service

  /**
   * Example:
   *
   * client.removeDevice(device._id, () => {
   *   console.log(`${device.name} has been removed`)
   * })
   *
   */
})

process.on('SIGINT', () => {
  // Do graceful exit
  client.close()
})


module.exports = plugin