'use strict'

let reekoh = require('reekoh')
let plugin = new reekoh.plugins.Connector()

/**
 * Emitted when device data is received.
 * This is the event to listen to in order to get real-time data feed from the connected devices.
 * @param {object} data The data coming from the device represented as JSON Object.
 */
plugin.on('data', (data) => {
  //  TODO: Send data outbound to the other platform, service or app here.
  console.log(data)
})

/**
 * Emitted when the platform bootstraps the plugin. The plugin should listen once and execute its init process.
 */
plugin.once('ready', () => {
  /*
   *  Initialize the connection using the plugin.config. See config.json
   *  You can customize config.json based on the needs of your plugin.
   *  Reekoh will inject these configuration parameters as plugin.config when the platform bootstraps the plugin.
   *
   *  Example:
   *
   *  let connectionString = plugin.config.connstring
   *  connection = service.connect(connectionString)
   *
   *  Note: Config Names are based on what you specify on the config.json.
   */
  console.log(plugin.config)
  plugin.log('Connector has been initialized.')
})

module.exports = plugin